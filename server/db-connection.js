const mysql = require('mysql');
// const connection = mysql.createPool({
//     connectionLimit: 100,
//     host:'103.27.74.63', // 'matrix2.sfdns.net'
//     user: 'macjakec_admin',
//     password: 'abc#123',
//     database: 'macjakec_cyberview',
//     debug: true 
// });
const connection = mysql.createPool({
    connectionLimit: 100,
    host:'localhost', // 'matrix2.sfdns.net'
    user: 'admin',
    password: 'abc#123', 
    debug: true

});


module.exports = connection;


//If you want to get user, you need start query in your mysql:
// SELECT user(); // output your user: root@localhost
// SELECT system_user(); // --

// //If you want to get port your "mysql://user:pass@hostname:port/db"
// SELECT @@port; //3306 is default

// //If you want hostname your db, you can execute query
// SELECT @@hostname;


